@extends('layouts.app')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
  <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <h1 class="panel-success">Category Lists</h1>
        
        		</div>
        	</div>
        </div>
 @foreach($category as $items)
        <div class="panel" style="width: 400px;margin-bottom: 1px;">
            <div class="panel">
                 <div style="text-align: center;">
                 	<div class="panel panel-default">
                     	  <div class="col-md-6">
                              Category Id: <h6 for="user" class="control-label custom">{{ $items->id }}</h6>
                          </div>
                          <div class="col-md-6">
                             Category Name:<h6 for="user" class="control-label custom">{{ $items->name }}</h6>
                          </div>
                      <a href="{{route('category.show',['id' => $items->id])}}"> 
                          <button type="submit" name="view" class="btn btn-info">
                              View post
                          </button>
                      </a>
            
                 	</div>
                 </div>
            </div>
      
        </div>
 @endforeach
@endsection