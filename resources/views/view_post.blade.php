@extends('layouts.app')
 <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
@section('content')
{{-- Heading post --}}


<div class="container">
        <div class="row">
            <div class="col-md-8 ">
               <div class="panel panel-default">
                 <h2 class="panel panel-success">View Post</h1>
        
        	   </div>
        	</div>
        </div>

{{-- Post view container --}}

 <div class="panel panel-success">
    <div class="panel-heading panel-input"  style="height: 300px;">
        <div class="col-md-8">
            <label>User name:</label><div >{{Auth::user()->name}}</div>
            <label>User Email:</label><div>{{Auth::user()->email}}</div>
        </div>
        <div class="col-md-8">
            <h2 for="user" class="control-label custom">{{ $post->post_title }}</h2>
        </div>
        <div class="col-md-8">
            <h6 for="user" class="control-label custom">{{ $post->descreption }}</h6>
        </div>

<img src="data:image/{{$post->post_image_type}};base64,{{base64_encode($post->post_image)}}" alt="image not found" style="width: 150px;height: 150px;">
        <div class="col-md-4" style="float: right;">
            <h6 for="user" class=" control-label custom"> posted on:{{ $post->created_at }}</h6>
            <h6 for="user" class=" control-label custom"> Category id:{{ $post->category_id }}</h6>
            <h6 for="user" class=" control-label custom"> User id:{{ $post->user_id }}</h6>
        </div>
      <div class="col-md-4" style="margin-top: 50px">
        @foreach($tags as $tag)
        <a style="font-size: 25px" href="">#{{$tag}}</a>
        @endforeach
      </div>

    </div>
</div>

    
{{-- Commend Box --}}

    <div class="container">
       {!! Form::open(['route' => 'commends.store']) !!}
       {!! Form::textarea('commends', null, ['size' => '30x4','class'=>'cust', 'placeholder'=>'Write somthing you wish', 'required']); !!}
       <input type="hidden" name="id" value="{{$post->id}}">
       </br>
        {{session()->put('key', $post->id)}}
        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
        {!! Form::close() !!}
    </div>


{{-- Commends by lists --}}

    @foreach($commends as $items)
     {!! Form::model($commends,['route' => ['commends.update', $items->id]]) !!}
        <div class="container">
           <div class="panel panel-info" style="text-align:inherit;">
                <a><b>{{Auth::user()->name}}</b></a> 
                @if (session('edit'))
                <input type="text" name="updatecmd" value="{{$items->descreption}}" />
                @else
                <p>{{$items->descreption}} </p>
                @endif
                <label>{{$items->created_at}}</label>


            <div class="" class="btn-primary btn" style="margin-top: 25px;float: right;">
       
            @if (session('edit'))
          
          {{-- update commend  --}}

               
                <input type="hidden" name="_method" value="PUT">                 
                      <button class="btn" type="submit">update commend</button>
                      
                      
                {!! Form::close() !!}
                @else
                <a href="{{route('commends.edit',['id' => $items->id])}}">edit commend</a> 
           @endif
            </div>

{{-- Delete commend link --}}

             {!! Form::open(['method' => 'DELETE', 
                'route' => ['commends.destroy', $items->id], 
                'id' => 'form-delete-commends-' . $items->id]) !!}
                <a href="" class="data-delete" 
                  data-form="commends-{{ $items->id }}">
                  delete</a>
              {!! Form::close() !!}
           </div>
        </div>
@endforeach

{{-- Scripts for deleteing commend by using link(href) --}}

<script>
$(function () {
  $('.data-delete').on('click', function (e) {
    if (!confirm('Are you sure you want to delete?')) return;
    e.preventDefault();
    $('#form-delete-' + $(this).data('form')).submit();
  });
});

</script>

@endsection
