@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">POSTS</div>
        
        {!! Form::model($post,['route' => ['posts.update', $post->id]]) !!}
            <input type="hidden" name="_method" value="PUT">
    
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                        <div class="form-group">
                            <label for="user" class="col-md-4 control-label">Post title</label>

                            <div class="col-md-6">
                                <input type="text" value="{{$post->post_title}}" class="form-control" name="post_title" required>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="user" class="col-md-4 control-label">Post Descreption</label>

                            <div class="col-md-6">
                                <input type="text" value="{{$post->descreption}}" class="form-control" name="descreption" required>
                            </div>
                       
                       <div class="form-group">
                        <label for="user" class="col-md-4 control-label">category</label>
                        <div class="col-md-6">
                      
                      {!! Form::select('categories',$categories); !!} 
      
                            </div>
                       </div>
                        </div>
                        <input type="submit" name="update" value="Update Post" class="btn btn-primary">  
                                    
                      
           {!! Form::close() !!}
                </div>




            </div>
        </div>
    </div>
</div>
@endsection