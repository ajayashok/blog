@extends('layouts.app')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" />
    <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                POSTS
                </br></br>
    @foreach($errors->all() as $message) 
    {{ $message }} </br>
      @endforeach
              </div>        
        {!! Form::open(['route' => 'posts.store','files'=>true]) !!}
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
         
                        <div class="form-group">
                            <label for="user" class="col-md-4 control-label">Post title</label>

                            <div class="col-md-6">
                              {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Enter Title'])!!}
                            </div>
                        </div>
                         <div class="form-group" style="margin-top: 40px">
                            <label for="user" class="col-md-4 control-label">Post Descreption</label>

                            <div class="col-md-6">
                        {!! Form::text('descreption',null,['class'=>'form-control','placeholder'=>'Enter Descreption'])!!}
                            </div>
                        </div>  
                       
                       <div class="form-group" style="margin-top: 40px">
                        <label for="user" class="col-md-4 control-label">category</label>
                        <div class="col-md-6">
                           @if($categories)
                              
                                {!! Form::select('categories', $categories); !!}
                            @endif
                        </div>
                       </div>
                           <br/><br/></br>
                       <div class="form-group" >
                              <label for="user" class="col-md-4 control-label">Tags:</label>
                         <div class="col-md-6">
                            <select data-role="tagsinput" type="text" multiple="multiple" name="tags[]" placeholder="Enter Tags" >
                            </select>
                        </div>
                      </div>  
            
              <div class="form-group">
                  <label for="user" class="col-md-4 control-label">Image Upload:</label>
                  <div class="row">
                      <div class="col-md-6">
                          {!! Form::file('image', array('class' => 'form-control')) !!}
                      </div>
                  </div>     
              </div>
                       <div class="col-md-6" style="margin-top: 40px">
                        <input type="submit" name="add" value="Add Post" class="btn btn-primary">  
                          </div>          
                      
           {!! Form::close() !!}
                </div>




            </div>
        </div>
    </div>
</div>
@endsection