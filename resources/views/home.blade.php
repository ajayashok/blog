@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
              <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                          
                        </div>
                    @endif
            </div>
      <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
              {{-- <div style="-webkit-border-after-style: inset;">Welcome User: {{Auth::user()->name}}</div> --}}
              {{-- <div style="-webkit-border-after-style: inset;" >Email: {{Auth::user()->email}}</div> --}}
             
        
           You are logged in!
         
          </div>

        
            <a href="posts/create">
                <button type="button" class="btn btn-primary">
                            Add Posts
                </button>
            </a>
        </br>
        </br>

        @foreach($posts as $items)
        <div class="panel panel-success">
            <div class="panel-heading panel-input">
                <div class="col-md-8">
                    <h2 for="user" class="control-label custom">{{ $items->post_title }}</h2>
                </div>
                <div class="col-md-8">
                <h6 for="user" class="control-label custom">{{ $items->descreption }}</h6>
                </div>

                  <div class="col-md-4" style="float: right;">
                    <h6 for="user" class=" control-label custom"> posted on:{{ $items->created_at }}</h6>
                    <h6 for="user" class=" control-label custom"> Category id:{{ $items->category_id }}</h6>
                    <h6 for="user" class=" control-label custom"> User id:{{ $items->user_id }}</h6>
                   </div>
                    

                  <div class="col-md-2" style="margin-top: 25px;">
                     <a href="{{route('posts.edit',['id' => $items->id])}}">  
                        <button class="btn-primary btn" type="submit">
                           Edit
                        </button>
                    </a>
                  </div>
                  <div class="col-md-2" style="margin-top: 25px;">
                     <a href="{{route('posts.show',['id' => $items->id])}}">  
                        <button class="btn-info btn" type="button">View</button>
                    </a>
                  </div>
                 
                  <div class="col-md-2" style="margin-top: 25px;">
                    <form action="{{url('posts', [$items->id])}}" method="POST">
                        {{method_field('DELETE')}}
                        {{csrf_field()}}
                        <button class="btn-danger btn" type="submit">
                            Delete
                        </button>
                    </form>
                  </div>
            
                
              
                </div>
            </div>
              @endforeach
        </div>
    </div>
</div>

@endsection
