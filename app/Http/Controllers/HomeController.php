<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 use App\Model\Post;
use View;
use Response;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return View::make('home')->with('posts', $posts);

         // return Response::json(['data' => $posts], 200);
         // return response() -> ($posts, 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }
}
