<?php

namespace App\Http\Controllers;

use App\Model\Commend;
use App\Model\Post;
use Illuminate\Http\Request;

class CommendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $commend =new Commend();
         $commend->post_id=$request->id;
         $commend->descreption=$request->commends;
         $commend->save();
         return redirect('/posts/'.$request->id);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Commend  $commend
     * @return \Illuminate\Http\Response
     */
    public function show(Commend $commend)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Commend  $commend
     * @return \Illuminate\Http\Response
     */
    public function edit(Commend $commend)
    {
         $value = session('key');
         session()->put('edit', 'edit');
         return redirect('/posts/'.$value);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Commend  $commend
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commend $commend)
    {
       session()->put('edit', '');
        $commend->descreption =$request->updatecmd;
        $commend->save();
        return redirect('/posts/'.session('key'))->with('status', 'Commend has been updated successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Commend  $commend
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commend $commend)
    {
         $value = session('key');
        $delete=Commend::Find($commend->id);
        //$delete->delete();
        dd($commend);
       // return redirect('/posts/'.$value)->with('status', 'Commend has been deleted!!');
   
    }
}
