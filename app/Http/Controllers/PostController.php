<?php

namespace App\Http\Controllers;

use App\Model\Post;
use App\Model\Category;
use App\Model\tag;
use App\Model\Commend;
use App\Model\PostTag;
use Auth;
use View;
use Response;
use App\Http\Requests\RequestPost;
use App\Http\Resources\PostResource;
use App\Http\Resources\PostCollection;

use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with(['tags'])->get();
      // return view('home',compact('posts',$posts));
        return PostResource::collection($posts);
        // return PostResource::collection($posts->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $categories = Category::pluck('name','id');
          return view('posts',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestPost $request)
    {
        

        $tmp_img = $request->image->path();
         $post = new Post;

        $post->user_id =Auth::user()->id;
        $post->category_id =$request->categories;
        $post->post_title = $request->title;
        $post->descreption =$request->descreption;
        $post->post_image =file_get_contents($tmp_img);
        $post->post_image_type =$request->image->extension();
        $post->save();

        $tags_arr=$request->tags;
        if($tags_arr){
        foreach ($tags_arr as $items) {
            $tag = new tag(['name' => $items]);
            $post->tags()->save($tag);
        }
    }
       
          $posts = Post::all();
          return View::make('home')->with('posts', $posts);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {

      $posts = Post::with(['tags'])->get();
      $tags=$posts->find($post->id)->tags->pluck('name');
      $commends=Commend::where('post_id',$post->id)->get();

      // return view('view_post',compact('post','commends','tags'));

    

     // return new PostResource(Post::find($post->id));
     return new PostResource($post);
      

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
         $categories = Category::pluck('name','id');
         $post = Post::where('id', $post->id)
                        ->first();
        return view('edit', compact('post','categories'));
         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {

        $post->category_id =$request->categories;
        $post->post_title = $request->post_title;
        $post->descreption =$request->descreption;
        $post->save();
        return redirect('/home')->with('status', 'Posts has been updated successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
       
       $delete=Post::Find($post->id);
       $delete->delete();
      return redirect('/home')->with('status', 'Posts has been deleted!!');
    }
}
