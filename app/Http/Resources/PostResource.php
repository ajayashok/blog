<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Model\Commend;

class PostResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

         return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'commends' => CommandsResource::collection($this->commends),
            'tags' => TagsResource::collection($this->tags),
            'category_id' => $this->category_id,
            'post_title' => $this->post_title,
            'descreption' => $this->descreption,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    
        // return parent::toArray($request);
    
    }
}
