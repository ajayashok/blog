<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CommandsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
             'id' => $this->id,
            'post_id' => $this->post_id,
            'descreption' => $this->descreption,
            'created_at' => $this->created_at->diffForHumans(),
            'updated_at' => $this->updated_at->diffForHumans(),

        ];
    }
}
