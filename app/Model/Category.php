<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
   /**Table name for posts */
    protected $table = 'categories';

 /**
     * Fillable attribute.
     */
    protected $fillable = ['name'];

    /* relatioship to posts */
    public function categoryPost()
    {
    	return $this->belongsTo('App\Model\Post');
    }
  
}
