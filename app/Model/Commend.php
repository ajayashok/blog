<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Commend extends Model
{
    public function posts()
    {
    	 return $this->belongsTo('App\Model\Post');              
    }
}
