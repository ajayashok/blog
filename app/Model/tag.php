<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class tag extends Model
{
   	/**Table name for posts */
    protected $table = 'tags';

    /**
     * Fillable attribute.
     */
    protected $fillable = ['name'];

     public function tags()
    {
      
    	return $this->belongsToMany('App\Model\Post')
                    ->withTimestamps();
    }

     
}
