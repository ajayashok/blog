<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

 class Post extends Model
{
	/**Table name for posts */
    protected $table = 'posts';

    /**
     * Fillable attribute.
     */
    protected $fillable = ['post_title','descreption'];

     public function category()
    {
    	return $this->hasOne('App\Model\Category');
    }

     public function user()
    {
    	return $this->belongsTo('App\Model\User');
    }
     public function tags()
    {
        return $this->belongsToMany('App\Model\tag')
                    ->withTimestamps();
    }
    public function commends()
    {
        return $this->hasMany('App\Model\Commend');  
    }
}
