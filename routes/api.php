<?php

use Illuminate\Http\Request;
use App\Model\User;
use App\Http\Resources\UserResource;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

    
});
Route::resource('posts','PostController');
Route::Apiresource('category','CategoryController');




// Route::get('/user', function () {
    // return new UserResource(User::find(2));
    // return UserResource::collection(User::all());
// });
// Route::get('/apipost', function () {
    // return new PostResource(Post::find(47));\
     //return PostResource::collection(Post::paginate());
//       return PostResource::collection(Post::all());
// });